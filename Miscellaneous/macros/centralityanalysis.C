
// Description:
//      
//       Analysis macro.
//
//
// Environment:
//      MPDROOT
//
// Author List:
//       Luis Valenzuela-Cazares          (original author)
//   
//-----------------------------------------------------------
#if !defined(__CINT__) && !defined(__CLING__)
#include "TString.h"
#include "TStopwatch.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TMath.h"

#include "MpdSts.h"
#include "TpcDetector.h"
#include "MpdBbc.h"
#include "MpdStrawendcap.h"
#include "MpdGetNumEvents.h"

#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>

#include <stdio.h>      /* printf */
#include <math.h>       /* tgamma */

using namespace std;

#endif

#include "/home/luis/mpdroot/macro/mpd/mpdloadlibs.C"

////////////////////////NBD fit/////////////////////////////////
  
  Double_t NBDFit(Double_t *x, Double_t *par){

  Int_t nhits=x[0];
  Int_t meanMul=par[0];
  Double_t k=par[1];
  Double_t acentors = par[2];

 return  acentors*(tgamma(nhits+k)/((tgamma(nhits+1))*(tgamma(k))))*(((TMath::Power(meanMul/k,nhits))/(TMath::Power(meanMul/k +1,nhits+k))));
  
 // return x;

}

 ////////////////////////NBD fit Ncol and Npar/////////////////////////////////
/*
  Double_t NBDFit(Double_t *x, Double_t *par){

  Int_t nhits=x[0];
  Int_t meanMul=par[0];
  Double_t k=par[1];
   Double_t f = par[2];
  Double_t Npar = par[3];
  Double_t Ncol = par[4];

 return  (f*Npar + (1-f)*Ncol)*(tgamma(nhits+k)/((tgamma(nhits+1))*(tgamma(k))))*(((TMath::Power(meanMul/k,nhits))/(TMath::Power(meanMul/k +1,nhits+k))));
  
 // return x;

}
*/

////////////////////////////////////////////////////////////////

/////////////////////////////////////////
  //Function to get the ring.
   Int_t GetRing(Int_t detID){

  if(detID >= 1 && detID <= 12) return 1;
  else if (detID>=13 && detID<=30) return 2;
  else if (detID>=31 && detID<= 54) return 3;
  else if (detID>=55 && detID<=84) return 4;
  else if (detID>=85 && detID<=120) return 5;
  else if (detID>=121 && detID<=162) return 6;
  
  else if (detID>=163 && detID<=174) return 1;
  else if (detID>=175 && detID<=192) return 2;
  else if (detID>=193 && detID<=216) return 3;
  else if (detID>=217 && detID<=246) return 4;
  else if (detID>=247 && detID<=282) return 5;
  else if (detID>=283 && detID<=324) return 6;

   else return -1;

}


//Int_t centrality(TString inputFile="/home/luis/Analysis-BEBE/evetest-bbcv5-5000AuAu9GeV-b0-12.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest.root")
Int_t centralityclasses(TString inputFile="/home/luis/Analysis-BEBE/10000AuAu11GeV16fm.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest.root")
{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gStyle->SetOptDate(0);      //show day and time
    gStyle->SetOptStat(1);      //show statistic
    gStyle->SetPalette(1,0);
    
    mpdloadlibs(); 

//////////////////////////////////Impact parameter///////////////////////////////////
   TH1F *hBmdb = new TH1F("hBmdb","Impact parameter",32,0,16);
   TH1F *hBmdbmul = new TH1F("hBmdbmul","Impact parameter multiplicity",1600,0,16);

///////////////////////////////////////////////Multiplicity charged particles///////////////////////////////////////////////// 
    TH1D *hBmdMul = new TH1D("hBmdMul","Charged particles multiplicity BMD A and C. 5000 Au+Au @9GeV UrQMD. MPDROOT.",100,0,100);
    hBmdMul->SetXTitle("BMD A and C");  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    TH1D *hMCTrackMul = new TH1D("hMCTrackMul","Charged particles multiplicity. 5000 Au+Au @9GeV UrQMD. MPDROOT.",150,0,150);
    hMCTrackMul->SetXTitle("Multiplicity");  

      
//Multiplicity for each range of impact parameter.
 TH1F *hrange1bbmd  = new TH1F("hrange1bbmd","Range 1 impact parameter, MPDROOT.",160,0,16);
       hrange1bbmd->SetXTitle("b(fm)");
       hrange1bbmd->SetYTitle("events");
    //  hrange1bbmd->SetYTitle("multiplicity");
    //     hrange1bbmd->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
    //   hrange1bbmd->SetMarkerColor(kBlack);
    //   hrange1bbmd->SetMarkerStyle(kDot);
       hrange1bbmd->SetLineColorAlpha(kBlack, 0.35);
       hrange1bbmd->GetXaxis()->CenterTitle(true);
       hrange1bbmd->GetXaxis()->SetTitleSize(0.04);
       hrange1bbmd->GetXaxis()->SetLabelSize(0.03);
       hrange1bbmd->GetXaxis()->SetTitleOffset(1.4);

 TH1F *hrange2bbmd  = new TH1F("hrange2bbmd","Range 2 impact parameter. MPDROOT.",160,0,16);
      // hrange2bbmd->SetMarkerColor(kRed);
      // hrange2bbmd->SetMarkerStyle(kPlus);
       hrange2bbmd->SetLineColorAlpha(kBlue, 0.35);

 TH1F *hrange3bbmd  = new TH1F("hrange3bbmd","Range 3 impact parameter. MPDROOT.",160,0,16);
      // hrange3bbmd->SetMarkerColor(kGreen);
      // hrange3bbmd->SetMarkerStyle(kStar);
        hrange3bbmd->SetLineColorAlpha(kGreen, 0.35);

 TH1F *hrange4bbmd  = new TH1F("hrange4bbmd","Range 4 impact parameter. MPDROOT.",160,0,16);
      // hrange4bbmd->SetMarkerColor(kMagenta);
      // hrange4bbmd->SetMarkerStyle(kStar);
         hrange4bbmd->SetLineColorAlpha(kMagenta, 0.35);

 TH1F *hrange5bbmd  = new TH1F("hrange5bbmd","Range 5 impact parameter. MPDROOT.",160,0,16);
       //hrange5bbmd->SetMarkerColor(kYellow);
       //hrange5bbmd->SetMarkerStyle(kPlus);
       hrange5bbmd->SetLineColorAlpha(kYellow, 0.35);

 TH1F *hrange6bbmd  = new TH1F("hrange6bbmd","Range 6 impact parameter. MPDROOT.",160,0,16);
      // hrange6bbmd->SetMarkerColor(kBlue);
      // hrange6bbmd->SetMarkerStyle(kDot);
       hrange6bbmd->SetLineColorAlpha(kBlue, 0.35);

 TH1F *hrange7bbmd  = new TH1F("hrange7bbmd","Range 7 impact parameter. MPDROOT.",160,0,16);
      // hrange7bbmd->SetMarkerColor(kBlue);
      // hrange7bbmd->SetMarkerStyle(kDot);
       hrange7bbmd->SetLineColorAlpha(kRed, 0.35);

  TH1F *hrange8bbmd  = new TH1F("hrange8bbmd","Range 8 impact parameter. MPDROOT.",160,0,16);
      // hrange8bbmd->SetMarkerColor(kBlue);
      // hrange8bbmd->SetMarkerStyle(kDot);
       hrange8bbmd->SetLineColorAlpha(kGreen, 0.35);

  TH1F *hrange9bbmd  = new TH1F("hrange9bbmd","Range 9 impact parameter. MPDROOT.",160,0,16);
      // hrange9bbmd->SetMarkerColor(kBlue);
      // hrange9bbmd->SetMarkerStyle(kDot);
       hrange9bbmd->SetLineColorAlpha(kBlack, 0.35);
 
 TH1F *hrange10bbmd  = new TH1F("hrange10bbmd","Range 10 impact parameter. MPDROOT.",160,0,16);
      // hrange10bbmd->SetMarkerColor(kBlue);
      // hrange10bbmd->SetMarkerStyle(kDot);
       hrange10bbmd->SetLineColorAlpha(kOrange, 0.35);
       
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 

    //Defining pointers to data
    
    TFile fileInput(inputFile.Data());
    
    TTree *simEvent = (TTree*) fileInput.Get("cbmsim");
    
    //Arrays/////name variable///////////intern memory name
    TClonesArray *bmdPoints = (TClonesArray*) fileInput.FindObjectAny("BmdPoint");   //Turn on for bmd
    simEvent->SetBranchAddress("BmdPoint",&bmdPoints);
    
 //   TClonesArray *bbcPoints = (TClonesArray*) fileInput.FindObjectAny("BBCPoint"); //Turn on for bbc
 //    simEvent->SetBranchAddress("BBCPoint",&bbcPoints);

    TClonesArray* mcTracks = (TClonesArray*) fileInput.FindObjectAny("MCTrack");
    simEvent->SetBranchAddress("MCTrack",&mcTracks);
  
 //  TClonesArray* tpcPoints = (TClonesArray*) fileInput.FindObjectAny("TpcPoint");
 //   simEvent->SetBranchAddress("TpcPoint",&tpcPoints);
  
    FairMCEventHeader* mcHeader = (FairMCEventHeader*) fileInput.FindObjectAny("MCEventHeader.")
;    simEvent->SetBranchAddress("MCEventHeader.",&mcHeader);
        
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Declaring variable for the impact parameter.
    Double_t impactParameter; //! impact parameter 

     TFile* fileOutput = new TFile(Form("%s",outputFile.Data()),"RECREATE");
   
     Int_t events  = simEvent->GetEntries();
  
    //Begins loop for the events
    for (Int_t i = 0000; i < events; ++i) {
    simEvent->GetEntry(i);

     MpdMCEventHeader *extraEventHeader = dynamic_cast<MpdMCEventHeader*> (mcHeader);

     impactParameter = extraEventHeader->GetB();
      
      hBmdb->Fill(impactParameter);
   //   hBmdbmul->Fill(impactParameter);

//    cout<<"Impact parameter: "<<extraEventHeader->GetB()<<endl;
   
    Int_t nMCTracks  =  mcTracks->GetEntriesFast();
    Int_t nprimary   =  mcHeader->GetNPrim(); 
  //  cout<<"nMCTracks "<<nMCTracks<<endl;

    Double_t B  = mcHeader->GetB();
  
    Int_t nChargedMultiplicityBMD=0;
    Int_t nChargedMul=0;

  Int_t nrange1=0;
  Int_t nrange2=0;
  Int_t nrange3=0;
  Int_t nrange4=0;
  Int_t nrange5=0;
  Int_t nrange6=0;
  Int_t nrange7=0;
  Int_t nrange8=0;
  Int_t nrange9=0;
  Int_t nrange10=0;

    if (bmdPoints != 0 ) {

    Int_t nbmdPoints =  bmdPoints->GetEntriesFast();
    if( nbmdPoints < 1 ) continue;
    
 /*     
    if (bbcPoints != 0 ) {
    Int_t nbbcPoints =  bbcPoints->GetEntriesFast();
    if( nbbcPoints < 1 ) continue;
 */        
    //cout<<"nbmdPoints: "<<nbmdPoints<<endl;
       
    //Loop for particles in both BMD.
   
      for (Int_t j = 0; j < nbmdPoints; j++) {
      BmdPoint *p1    = (BmdPoint*) bmdPoints->At(j);
   //   for (Int_t j = 0; j < nbbcPoints; j++) {
   //   MpdBbcPoint *p1    = (MpdBbcPoint*) bbcPoints->At(j);
      TVector3 recMom;
      Int_t    trkID = p1->GetTrackID();
      Int_t detID =  p1->GetDetectorID();
    //    cout<<"detID: "<<detID<<endl;            

     Int_t x = p1->GetX();
     Int_t y = p1->GetY();
     Int_t z = p1->GetZ();

   //  cout<<"x: "<<x<<"y: "<<y<<"z: "<<z<<endl;   

        FairMCTrack* mcTr = (FairMCTrack*) mcTracks->UncheckedAt(trkID);

          Double_t Xv = mcTr->GetStartX();
          Double_t Yv = mcTr->GetStartY();
          Double_t Zv = mcTr->GetStartZ();

   Int_t ring = -1;
   ring = GetRing(detID);

////////////////////Primary Particles Condition////////////////////////////////////////////////// 
  if( mcTr->GetMotherId() < 0 ) {

   //Charged primary particles
         if ( TMath::Abs(mcTr->GetPdgCode() )  != 211  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 11   &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 2212 && //Protons
                  TMath::Abs(mcTr->GetPdgCode() )  != 321  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 13   &&
                  (TMath::Abs(Xv) > 0.001 || TMath::Abs(Yv) > 0.001 || TMath::Abs(Zv) > 0.01 )) continue;                    
                    
                 //if(ring==2 ||ring==3 || ring==4 || ring==5 || ring ==6 ){  
                   if(ring==3 || ring==4 || ring==5 || ring ==6 ){ 
               //  if( ring==4 || ring==5 || ring ==6 ){  
               //     if(ring==5 || ring ==6 ){  

               //  cout<<"ring: "<<ring<<endl;

                       //  if (  impactParameter < 12){
                            nChargedMultiplicityBMD++; 
                      //   }  

                    hBmdbmul->Fill(impactParameter);

                //  nChargedMul++;
 
               } //Rings  

   }


}  //Primary Particles Condition 

}

//Multiplicity charged particles
     hBmdMul->Fill(nChargedMultiplicityBMD);  

    // hMCTrackMul->Fill(nChargedMul);
    
  // cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<"impactParameter:"<<impactParameter<<endl;
  //  cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<endl;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////Impact parameter ranges for centrality classes///////////////////////////////////////////
//hBmdbmul->Fill(impactParameter);

//First range
if (0<=impactParameter && impactParameter<3.8595) {
     nrange1++;
     hrange1bbmd->Fill(impactParameter);
              }
//Second range
if (3.8595<=impactParameter && impactParameter<5.2105) {
  nrange2++;
  hrange2bbmd->Fill(impactParameter);
              }
//Third range
if (5.2105<=impactParameter && impactParameter<6.4405) {
  nrange3++;
  hrange3bbmd->Fill(impactParameter);
              }
//Forth range
if (6.4405<=impactParameter && impactParameter<7.5105) {
  nrange4++;
  hrange4bbmd->Fill(impactParameter);
              }
//Fifth range
if (7.5105<=impactParameter && impactParameter<8.5605) {
  nrange5++;
  hrange5bbmd->Fill(impactParameter);
              }
//Sixth range
if (8.5605<=impactParameter && impactParameter<9.7105) {
  nrange6++;
  hrange6bbmd->Fill(impactParameter);
              }
 //Seventh range
if (9.7105<=impactParameter && impactParameter<10.9095) {
  nrange7++;
  hrange7bbmd->Fill(impactParameter);
              }
//Eight range
if (10.9095<=impactParameter && impactParameter<12.3505) {
  nrange8++;
  hrange8bbmd->Fill(impactParameter);
              }
//Nineth range
if (12.3505<=impactParameter && impactParameter<13.9795) {
  nrange9++;
  hrange9bbmd->Fill(impactParameter);
              }
//Tenth range
if (13.9795<=impactParameter && impactParameter<15.8005) {
  nrange10++;
  hrange10bbmd->Fill(impactParameter);
              }   
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
  
/////////////////////////////NBD fit///////////////////////////////////////////////////////

  TF1 *Puk = new TF1("Binomial","NBDFit",0,90,3);
  //Puk->Draw();
  Puk->SetParameter(0,0.4);
  Puk->SetParameter(1,0.75);
  Puk->SetParameter(2,1000);
 // Puk->SetParameter(3,1000);
 // Puk->SetParameter(4,2000);
  //Puk->SetParLimits(1,0.75,2.0);
  //Puk->SetParLimits(0,20,50);

  hBmdMul->Fit("Binomial","M");

//  hMCTrackMul->Fit("Binomial","R");

  cout<<"NBD: "<<Puk<<endl;    

///////////////////////////////////////////////////////////////////////////////////////////


  } //End of the loop's event. 
  
///////////////////////////////////////////////  
   cout<<"Saving histograms"<<endl;
  
   
   fileOutput->mkdir("Mc");
   fileOutput->cd("Mc");
   
  gStyle->SetOptTitle(0); //No title for histograms


/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Impact parameter analysis//////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

TCanvas *c50 = new TCanvas("c50","Impact parameter (fm)");
hBmdb->SetStats(kFALSE);
hBmdb->GetXaxis()->SetTitle("b(fm)");
hBmdb->GetYaxis()->SetTitle("events number");
//hBmdb->Scale(1./hBmdb->Integral());
hBmdb->Draw();

TCanvas *c54 = new TCanvas("c54","Charged particles multiplicity BMD ");
hBmdMul->SetStats(kFALSE);
hBmdMul->GetXaxis()->SetTitle("Multiplicity");
//hBmdMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hBmdMul->GetYaxis()->SetTitle("Events number");
//hBmdMul->Sumw2();
//hBmdMul->Rebin();
//hBmdMul->Scale(1./hBmdMul->Integral());
hBmdMul->Draw();

TCanvas *c55 = new TCanvas("c55","Charged particles multiplicity BMD ");
hMCTrackMul->SetStats(kFALSE);
hMCTrackMul->GetXaxis()->SetTitle("Multiplicity");
//hMCTrackMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hMCTrackMul->GetYaxis()->SetTitle("Events number");
//hMCTrackMul->Sumw2();
//hMCTrackMul->Rebin();
//hMCTrackMul->Scale(1./hBmdMul->Integral());
hMCTrackMul->Draw();

 
TCanvas *c53 = new TCanvas("c53","Impact parameter (fm)");
hBmdbmul->SetStats(kFALSE);
hBmdbmul->GetXaxis()->SetTitle("b(fm)");
hBmdbmul->GetYaxis()->SetTitle("Multiplicity");
//hBmdbmul->Scale(1./hBmdb->Integral());
hBmdbmul->Draw();

//Int_t integralMulb = hBmdbmul->Integral();
// cout<<"integralMulb: "<<integralMulb<<endl;  

/*
//////////////////////////////////////////Finding impact parameter ranges for centrality classes/////////////////////////
 Double_t a = 0;
 Double_t fraction =0;
 Double_t epsilon = 0.001; 
 Int_t totalIntegral = hBmdbmul->Integral(hBmdbmul->FindFixBin(0), hBmdbmul->FindFixBin(12));
 Double_t c1 = 0.1;
 Double_t c2 = 0.2;
 Double_t c3 = 0.3;
 Double_t c4 = 0.4;
 Double_t c5 = 0.5;
 Double_t c6 = 0.6;
 Double_t c7 = 0.7;
 Double_t c8 = 0.8;
 Double_t c9 = 0.9;
 Double_t c10 = 1.0;

 Double_t b1 = 0;
 Double_t b2 = 0;
 Double_t b3 = 0;
 Double_t b4 = 0;
 Double_t b5 = 0;
 Double_t b6 = 0;
 Double_t b7 = 0;
 Double_t b8 = 0;
 Double_t b9 = 0;
 Double_t b10 = 0;

Int_t bBin = hBmdbmul->FindFixBin(a);

              while (   TMath::Abs(fraction-c1) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;    
              } 
              b1= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

             while (   TMath::Abs(fraction-c2) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
               b2= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c3) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b3= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c4) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b4= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c5) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b5= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c6) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b6= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c7) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b7= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c8) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b8= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c9) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              }
              b9= hBmdbmul->GetXaxis()->GetBinCenter(bBin);
 
                while (   TMath::Abs(fraction-c10) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b10= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

               cout<<"b1: "<<b1<<endl; 
               cout<<"b2: "<<b2<<endl;  
               cout<<"b3: "<<b3<<endl; 
               cout<<"b4: "<<b4<<endl; 
               cout<<"b5: "<<b5<<endl; 
               cout<<"b6: "<<b6<<endl; 
               cout<<"b7: "<<b7<<endl; 
               cout<<"b8: "<<b8<<endl; 
               cout<<"b9: "<<b9<<endl; 
               cout<<"b10: "<<b10<<endl; 
 
               cout<<"fraction: "<<fraction<<endl;  
               cout<<"bBin: "<<bBin<<endl; 
               cout<<"epsilon: "<<epsilon<<endl;   
               cout<<"bin number : "<<hBmdbmul->FindFixBin(bBin)<<endl;  
               cout<<"totalIntegral: "<<totalIntegral<<endl;  
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

   //Centrality classes of 0-5, 5-20, 20-30, 30-40, 40-50 and 50-60 percentages.
   TCanvas *c12 = new TCanvas("c12","Impact parameter ranges",800,800);
   gStyle->SetOptStat(false);                                   
   //gStyle->SetPalette(1);                                       
   c12->SetRightMargin(0.0465116);
   c12->SetTopMargin(0.1);
   c12->SetFillColor(0);
/*
   hrange1bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange2bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange3bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange4bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange5bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange6bbmd->Scale(1.0/hBmdbmul->Integral());
*/
   hrange1bbmd->Draw();  
   hrange2bbmd->Draw("sames");  
   hrange3bbmd->Draw("sames");  
   hrange4bbmd->Draw("sames");   
   hrange5bbmd->Draw("sames");  
   hrange6bbmd->Draw("sames"); 
   hrange7bbmd->Draw("sames"); 
   hrange8bbmd->Draw("sames"); 
   hrange9bbmd->Draw("sames"); 
   hrange10bbmd->Draw("sames"); 
  
    TLegend *leg = new TLegend(0.65,0.8,0.92,0.89);
   leg->SetTextFont(62);
  // leg->SetTextSize(1);                                    
   leg->SetLineColor(0);
   leg->SetLineStyle(0);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   leg->AddEntry("","Impact parameter ranges","");
   leg->AddEntry("hrange1bbmd","class 1","p");
   leg->AddEntry("hrange2bbmd","class 2","p");
   leg->AddEntry("hrange3bbmd","class 3","p");
   leg->AddEntry("hrange4bbmd","class 4","p"); 
   leg->AddEntry("hrange5bbmd","class 5","p");
   leg->AddEntry("hrange6bbmd","class 6","p");
   leg->AddEntry("hrange7bbmd","class 7","p");
   leg->AddEntry("hrange8bbmd","class 8","p");
   leg->AddEntry("hrange9bbmd","class 9","p");
   leg->AddEntry("hrange10bbmd","class 10","p");
   leg->Draw();


   c12->SaveAs("bclasses.pdf");

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  cout<<"End histograms"<<endl;
  return 0;
   
}

