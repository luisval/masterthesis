
// Description:
//      
//       Analysis macro. This macro determines the ranges of impact parameter for each centrality class. It also estimates
//       the ranges of multiplicity for each centrality-multiplicity class.
//
// Environment:
//      MPDROOT
//
// Author List:
//       Luis Valenzuela-Cazares          (original author)
//   
//-----------------------------------------------------------
#if !defined(__CINT__) && !defined(__CLING__)
#include "TString.h"
#include "TStopwatch.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TMath.h"

#include "MpdGetNumEvents.h"

#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>

#include <stdio.h>      /* printf */
#include <math.h>       /* tgamma */

using namespace std;

#endif

#include "/home/luis/mpdroot/macro/mpd/mpdloadlibs.C"

////////////////////////////////////////////////////////////////

/////////////////////////////////////////
  //Function to get the ring.
   Int_t GetRing(Int_t detID){

  if(detID >= 1 && detID <= 12) return 1;
  else if (detID>=13 && detID<=30) return 2;
  else if (detID>=31 && detID<= 54) return 3;
  else if (detID>=55 && detID<=84) return 4;
  else if (detID>=85 && detID<=120) return 5;
  else if (detID>=121 && detID<=162) return 6;
  
  else if (detID>=163 && detID<=174) return 1;
  else if (detID>=175 && detID<=192) return 2;
  else if (detID>=193 && detID<=216) return 3;
  else if (detID>=217 && detID<=246) return 4;
  else if (detID>=247 && detID<=282) return 5;
  else if (detID>=283 && detID<=324) return 6;

   else return -1;

}


//Int_t centrality(TString inputFile="/home/luis/Analysis-BEBE/evetest-bbcv5-5000AuAu9GeV-b0-12.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest.root")
Int_t MCcentralityclasses(TString inputFile="/home/luis/Analysis-BEBE/10000AuAu11GeV16fm.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest.root")
{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gStyle->SetOptDate(0);      //show day and time
    gStyle->SetOptStat(1);      //show statistic
    gStyle->SetPalette(1,0);
    
//    mpdloadlibs(); 

//////////////////////////////////Impact parameter///////////////////////////////////
   TH1F *hBmdb = new TH1F("hBmdb","Impact parameter",32,0,16);
   TH1F *hBmdbmul = new TH1F("hBmdbmul","Impact parameter multiplicity",16000,0,16);


///////////////////////////////////////////////Multiplicity charged particles///////////////////////////////////////////////// 
    TH1D *hBmdMul = new TH1D("hBmdMul","Charged particles multiplicity BMD A and C. 5000 Au+Au @9GeV UrQMD. MPDROOT.",100,0,100);
    hBmdMul->SetXTitle("BMD A and C");  

     TH1D *hBmdNoMul = new TH1D("hBmdNoMul","Charged particles multiplicity BMD A and C. 5000 Au+Au @9GeV UrQMD. MPDROOT.",1000,0,100);
    hBmdNoMul->SetXTitle("BMD A and C");  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    TH1D *hMCTrackMul = new TH1D("hMCTrackMul","Charged particles multiplicity. 5000 Au+Au @9GeV UrQMD. MPDROOT.",150,0,150);
    hMCTrackMul->SetXTitle("Multiplicity");  
      
//Multiplicity for each range of impact parameter.
 TH1F *hrange1bbmd  = new TH1F("hrange1bbmd","Range 1 impact parameter, MPDROOT.",160,0,16);
       hrange1bbmd->SetXTitle("b(fm)");
       hrange1bbmd->SetYTitle("events");
    //  hrange1bbmd->SetYTitle("multiplicity");
    //     hrange1bbmd->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
    //   hrange1bbmd->SetMarkerColor(kBlack);
    //   hrange1bbmd->SetMarkerStyle(kDot);
       hrange1bbmd->SetLineColorAlpha(kBlack, 0.35);
       hrange1bbmd->GetXaxis()->CenterTitle(true);
       hrange1bbmd->GetXaxis()->SetTitleSize(0.04);
       hrange1bbmd->GetXaxis()->SetLabelSize(0.03);
       hrange1bbmd->GetXaxis()->SetTitleOffset(1.4);

 TH1F *hrange2bbmd  = new TH1F("hrange2bbmd","Range 2 impact parameter. MPDROOT.",160,0,16);
       hrange2bbmd->SetLineColorAlpha(kBlue, 0.35);

 TH1F *hrange3bbmd  = new TH1F("hrange3bbmd","Range 3 impact parameter. MPDROOT.",160,0,16);
       hrange3bbmd->SetLineColorAlpha(kGreen, 0.35);

 TH1F *hrange4bbmd  = new TH1F("hrange4bbmd","Range 4 impact parameter. MPDROOT.",160,0,16);
       hrange4bbmd->SetLineColorAlpha(kMagenta, 0.35);

 TH1F *hrange5bbmd  = new TH1F("hrange5bbmd","Range 5 impact parameter. MPDROOT.",160,0,16);
       hrange5bbmd->SetLineColorAlpha(kYellow, 0.35);

 TH1F *hrange6bbmd  = new TH1F("hrange6bbmd","Range 6 impact parameter. MPDROOT.",160,0,16);
       hrange6bbmd->SetLineColorAlpha(kBlue, 0.35);

 TH1F *hrange7bbmd  = new TH1F("hrange7bbmd","Range 7 impact parameter. MPDROOT.",160,0,16);
       hrange7bbmd->SetLineColorAlpha(kRed, 0.35);

  TH1F *hrange8bbmd  = new TH1F("hrange8bbmd","Range 8 impact parameter. MPDROOT.",160,0,16);
       hrange8bbmd->SetLineColorAlpha(kGreen, 0.35);

  TH1F *hrange9bbmd  = new TH1F("hrange9bbmd","Range 9 impact parameter. MPDROOT.",160,0,16);
       hrange9bbmd->SetLineColorAlpha(kBlack, 0.35);
 
 TH1F *hrange10bbmd  = new TH1F("hrange10bbmd","Range 10 impact parameter. MPDROOT.",160,0,16);
       hrange10bbmd->SetLineColorAlpha(kOrange, 0.35);
       
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
/////////////////////////////////////Multiplicity classes.///////////////////////////////////////////////////////////
 TH1F *hmulclass1  = new TH1F("hmulclass1","Multiplicity class 1, MPDROOT.",100,0,100);
       hmulclass1->SetXTitle("multiplicity N");
       hmulclass1->SetYTitle("events");
       hmulclass1->SetLineColorAlpha(kBlack, 0.35);
       hmulclass1->GetXaxis()->CenterTitle(true);
       hmulclass1->GetXaxis()->SetTitleSize(0.04);
       hmulclass1->GetXaxis()->SetLabelSize(0.03);
       hmulclass1->GetXaxis()->SetTitleOffset(1.4);

 TH1F *hmulclass2  = new TH1F("hmulclass2","Multiplicity class 2. MPDROOT.",100,0,100);
       hmulclass2->SetLineColorAlpha(kBlue, 0.35);

 TH1F *hmulclass3  = new TH1F("hmulclass3","Multiplicity class 3. MPDROOT.",100,0,100);
       hmulclass3->SetLineColorAlpha(kGreen, 0.35);

 TH1F *hmulclass4  = new TH1F("hmulclass4","Multiplicity class 4. MPDROOT.",100,0,100);
       hmulclass4->SetLineColorAlpha(kMagenta, 0.35);

 TH1F *hmulclass5  = new TH1F("hmulclass5","Multiplicity class 5. MPDROOT.",100,0,100);
       hmulclass5->SetLineColorAlpha(kYellow, 0.35);

 TH1F *hmulclass6  = new TH1F("hmulclass6","Multiplicity class 6. MPDROOT.",100,0,100);
       hmulclass6->SetLineColorAlpha(kBlue, 0.35);

 TH1F *hmulclass7  = new TH1F("hmulclass7","Multiplicity class 7. MPDROOT.",100,0,100);
       hmulclass7->SetLineColorAlpha(kRed, 0.35);

  TH1F *hmulclass8  = new TH1F("hmulclass8","Multiplicity class 8. MPDROOT.",100,0,100);
        hmulclass8->SetLineColorAlpha(kGreen, 0.35);

  TH1F *hmulclass9  = new TH1F("hmulclass9","Multiplicity class 9. MPDROOT.",100,0,100);
        hmulclass9->SetLineColorAlpha(kBlack, 0.35);
 
 TH1F *hmulclass10  = new TH1F("hmulclass10","Multiplicity class 10. MPDROOT.",100,0,100);
       hmulclass10->SetLineColorAlpha(kOrange, 0.35);
       
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
 

    //Defining pointers to data
    
    TFile fileInput(inputFile.Data());
    
    TTree *simEvent = (TTree*) fileInput.Get("cbmsim");
    
    //Arrays/////name variable///////////intern memory name
    TClonesArray *bmdPoints = (TClonesArray*) fileInput.FindObjectAny("BmdPoint");   //Turn on for bmd
    simEvent->SetBranchAddress("BmdPoint",&bmdPoints);
    
 //   TClonesArray *bbcPoints = (TClonesArray*) fileInput.FindObjectAny("BBCPoint"); //Turn on for bbc
 //    simEvent->SetBranchAddress("BBCPoint",&bbcPoints);

    TClonesArray* mcTracks = (TClonesArray*) fileInput.FindObjectAny("MCTrack");
    simEvent->SetBranchAddress("MCTrack",&mcTracks);
   
    FairMCEventHeader* mcHeader = (FairMCEventHeader*) fileInput.FindObjectAny("MCEventHeader.")
;    simEvent->SetBranchAddress("MCEventHeader.",&mcHeader);
        
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Declaring variable for the impact parameter.
    Double_t impactParameter; //! impact parameter 

     TFile* fileOutput = new TFile(Form("%s",outputFile.Data()),"RECREATE");
   
     Int_t events  = simEvent->GetEntries();
  
    //Begins loop for the events
    for (Int_t i = 0000; i < events; ++i) {
    simEvent->GetEntry(i);

     MpdMCEventHeader *extraEventHeader = dynamic_cast<MpdMCEventHeader*> (mcHeader);

     impactParameter = extraEventHeader->GetB();
      
      hBmdb->Fill(impactParameter);
   //   hBmdbmul->Fill(impactParameter);

//    cout<<"Impact parameter: "<<extraEventHeader->GetB()<<endl;
   
    Int_t nMCTracks  =  mcTracks->GetEntriesFast();
    Int_t nprimary   =  mcHeader->GetNPrim(); 
  //  cout<<"nMCTracks "<<nMCTracks<<endl;

    Double_t B  = mcHeader->GetB();
  
    Int_t nChargedMultiplicityBMD=0;
    Int_t nChargedMul=0;

  Int_t nrange1=0;
  Int_t nrange2=0;
  Int_t nrange3=0;
  Int_t nrange4=0;
  Int_t nrange5=0;
  Int_t nrange6=0;
  Int_t nrange7=0;
  Int_t nrange8=0;
  Int_t nrange9=0;
  Int_t nrange10=0;

  Int_t mulrange1=0;
  Int_t mulrange2=0;
  Int_t mulrange3=0;
  Int_t mulrange4=0;
  Int_t mulrange5=0;
  Int_t mulrange6=0;
  Int_t mulrange7=0;
  Int_t mulrange8=0;
  Int_t mulrange9=0;
  Int_t mulrange10=0;

   
////////////////////////////////Monte Carlo////////////////////////////////////////////////
//////////////////////////////////MCTrack/////////////////////////////////////////////////


    //Loop for particles
    for(Int_t k = 0;  k< nMCTracks; k++){
        
         TVector3 mcMom;
        
         FairMCTrack* mcTr = (FairMCTrack*) mcTracks->UncheckedAt(k);
         
         mcTr->GetMomentum(mcMom);

           Double_t Xv = mcTr->GetStartX();
           Double_t Yv = mcTr->GetStartY();
           Double_t Zv = mcTr->GetStartZ();

  if( mcTr->GetMotherId() < 0 ) {
             

         //Charged primary particles

        if ( TMath::Abs(mcTr->GetPdgCode() )  != 211  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 11   &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 2212 && //Protons
                  TMath::Abs(mcTr->GetPdgCode() )  != 321  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 13   &&
       (TMath::Abs(Xv) > 0.001 || TMath::Abs(Yv) > 0.001 || TMath::Abs(Zv) > 0.01 )) continue;
  
   

         //   if( TMath::Abs(mcMom.Eta()) >  2.2 && TMath::Abs(mcMom.Eta()) < 3.5) {          //Pseudorapidity region BE-BE   

  // if( TMath::Abs(mcMom.Eta()) > 3.0 && TMath::Abs(mcMom.Eta()) < 3 ) {   //Rings 4, 5 and 6.
                 
                    nChargedMultiplicityBMD++; 
               

                    hBmdbmul->Fill(impactParameter);
                    hBmdNoMul->Fill(nChargedMultiplicityBMD);

        //       } 
                   
       }

     }
/////////////////////////////////////////////////////////////////////// 


//Multiplicity charged particles
     hBmdMul->Fill(nChargedMultiplicityBMD);  

    // hMCTrackMul->Fill(nChargedMul);
    
  // cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<"impactParameter:"<<impactParameter<<endl;
  //  cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<endl;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////Multiplicity centrality classes///////////////////////////////////////////
//hBmdbmul->Fill(impactParameter);
/*
//First range
if (0<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<10) {
     mulrange1++;
     hmulclass1->Fill(nChargedMultiplicityBMD);
              }
//Second range
if (10<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<19) {
  mulrange2++;
  hmulclass2->Fill(nChargedMultiplicityBMD);
              }
//Third range
if (19<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<28) {
  mulrange3++;
  hmulclass3->Fill(nChargedMultiplicityBMD);
              }
//Forth range
if (28<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<37) {
  mulrange4++;
  hmulclass4->Fill(nChargedMultiplicityBMD);
              }
//Fifth range
if (37<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<46) {
  mulrange5++;
  hmulclass5->Fill(nChargedMultiplicityBMD);
              }
//Sixth range
if (46<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<56) {
  mulrange6++;
  hmulclass6->Fill(nChargedMultiplicityBMD);
              }
 //Seventh range
if (56<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<68) {
  mulrange7++;
  hmulclass7->Fill(nChargedMultiplicityBMD);
              }
//Eight range
if (68<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<84) {
  mulrange8++;
  hmulclass8->Fill(nChargedMultiplicityBMD);
              }
//Nineth range
if (84<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<99) {
  mulrange9++;
  hmulclass9->Fill(nChargedMultiplicityBMD);
              }
//Tenth range
if (99<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<100) {
  mulrange10++;
  hmulclass10->Fill(nChargedMultiplicityBMD);
              }   
              */
/////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////Impact parameter ranges for centrality classes///////////////////////////////////////////
//hBmdbmul->Fill(impactParameter);

//First range
if (0<=impactParameter && impactParameter<3.0495) {
     nrange1++;
     hrange1bbmd->Fill(impactParameter);
              }
//Second range
if (3.0495<=impactParameter && impactParameter<4.1505) {
  nrange2++;
  hrange2bbmd->Fill(impactParameter);
              }
//Third range
if (4.1505<=impactParameter && impactParameter<5.0595) {
  nrange3++;
  hrange3bbmd->Fill(impactParameter);
              }
//Forth range
if (5.0595<=impactParameter && impactParameter<5.9405) {
  nrange4++;
  hrange4bbmd->Fill(impactParameter);
              }
//Fifth range
if (5.9405<=impactParameter && impactParameter<6.7505) {
  nrange5++;
  hrange5bbmd->Fill(impactParameter);
              }
//Sixth range
if (6.7505<=impactParameter && impactParameter<7.5995) {
  nrange6++;
  hrange6bbmd->Fill(impactParameter);
              }
 //Seventh range
if (7.5995<=impactParameter && impactParameter<8.4395) {
  nrange7++;
  hrange7bbmd->Fill(impactParameter);
              }
//Eight range
if (8.4395<=impactParameter && impactParameter<9.4895) {
  nrange8++;
  hrange8bbmd->Fill(impactParameter);
              }
//Nineth range
if (9.4895<=impactParameter && impactParameter<10.7895) {
  nrange9++;
  hrange9bbmd->Fill(impactParameter);
              }
//Tenth range
if (10.7895<=impactParameter && impactParameter<15.0005) {
  nrange10++;
  hrange10bbmd->Fill(impactParameter);
              }   
/////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
  
  } //End of the loop's event. 
  
///////////////////////////////////////////////  
   cout<<"Saving histograms"<<endl;
  
   
   fileOutput->mkdir("Mc");
   fileOutput->cd("Mc");
   
  gStyle->SetOptTitle(0); //No title for histograms


/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Impact parameter analysis//////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

TCanvas *c50 = new TCanvas("c50","Impact parameter (fm)");
hBmdb->SetStats(kFALSE);
hBmdb->GetXaxis()->SetTitle("b(fm)");
hBmdb->GetYaxis()->SetTitle("events number");
//hBmdb->Scale(1./hBmdb->Integral());
hBmdb->Draw();

TCanvas *c54 = new TCanvas("c54","Charged particles multiplicity BMD ");
hBmdMul->SetStats(kFALSE);
hBmdMul->GetXaxis()->SetTitle("Multiplicity");
//hBmdMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hBmdMul->GetYaxis()->SetTitle("Events number");
//hBmdMul->Sumw2();
//hBmdMul->Rebin();
//hBmdMul->Scale(1./hBmdMul->Integral());
hBmdMul->Draw();

TCanvas *c56 = new TCanvas("c56","Charged particles multiplicity BMD ");
hBmdNoMul->SetStats(kFALSE);
hBmdNoMul->GetXaxis()->SetTitle("Multiplicity");
//hBmdNoMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hBmdNoMul->GetYaxis()->SetTitle("Number of times");
hBmdNoMul->Draw();

/*
TCanvas *c55 = new TCanvas("c55","Charged particles multiplicity MCTrack ");
hMCTrackMul->SetStats(kFALSE);
hMCTrackMul->GetXaxis()->SetTitle("Multiplicity");
//hMCTrackMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hMCTrackMul->GetYaxis()->SetTitle("Events number");
//hMCTrackMul->Sumw2();
//hMCTrackMul->Rebin();
//hMCTrackMul->Scale(1./hMCTrackMul->Integral());
hMCTrackMul->Draw();
*/
 
TCanvas *c53 = new TCanvas("c53","Impact parameter (fm)");
hBmdbmul->SetStats(kFALSE);
hBmdbmul->GetXaxis()->SetTitle("b(fm)");
hBmdbmul->GetYaxis()->SetTitle("Multiplicity");
//hBmdbmul->Scale(1./hBmdb->Integral());
hBmdbmul->Draw();

//Int_t integralMulb = hBmdbmul->Integral();
 //cout<<"integralMulb: "<<integralMulb<<endl;  


//////////////////////////////////////////Multplicity classes//////////////////////////////////////////////////////////
 /*
 Double_t am = 0;
 Double_t fractionmul =100;
 Double_t epsilonmul = 0.1; 
 Int_t totalIntegralMul = hBmdNoMul->Integral(hBmdNoMul->FindFixBin(0), hBmdNoMul->FindFixBin(100));
 Double_t cm1 = 0.1;
 Double_t cm2 = 0.2;
 Double_t cm3 = 0.3;
 Double_t cm4 = 0.4;
 Double_t cm5 = 0.5;
 Double_t cm6 = 0.6;
 Double_t cm7 = 0.7;
 Double_t cm8 = 0.8;
 Double_t cm9 = 0.9;
 Double_t cm10 = 1.0;

 Double_t m1 = 100;
 Double_t m2 = 100;
 Double_t m3 = 100;
 Double_t m4 = 100;
 Double_t m5 = 100;
 Double_t m6 = 100;
 Double_t m7 = 100;
 Double_t m8 = 100;
 Double_t m9 = 100;
 Double_t m10 = 100;

Int_t mBin = hBmdNoMul->FindFixBin(am);

              while (   TMath::Abs(fractionmul-cm1) > epsilonmul && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;    
              } 
              m1= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

             while (   TMath::Abs(fractionmul-cm2) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul = hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
               m2= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm3) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m3= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm4) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m4= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm5) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m5= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm6) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m6= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm7) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m7= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm8) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m8= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm9) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              }
              m9= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);
 
                while (   TMath::Abs(fractionmul-cm10) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m10= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

               cout<<"m1: "<<m1<<endl; 
               cout<<"m2: "<<m2<<endl;  
               cout<<"m3: "<<m3<<endl; 
               cout<<"m4: "<<m4<<endl; 
               cout<<"m5: "<<m5<<endl; 
               cout<<"m6: "<<m6<<endl; 
               cout<<"m7: "<<m7<<endl; 
               cout<<"m8: "<<m8<<endl; 
               cout<<"m9: "<<m9<<endl; 
               cout<<"m10: "<<m10<<endl; 
 
               cout<<"fractionmul: "<<fractionmul<<endl;  
               cout<<"mBin: "<<mBin<<endl; 
               cout<<"epsilonmul: "<<epsilonmul<<endl;   
               cout<<"bin number : "<<hBmdNoMul->FindFixBin(mBin)<<endl;  
               cout<<"totalIntegralMul: "<<totalIntegralMul<<endl;  

    */          
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////Centrality classes impact parameter//////////////////////////////////////////////////////////

 Double_t a = 0;
 Double_t fraction =100;
 Double_t epsilon = 0.001; 
 Int_t totalIntegral = hBmdbmul->Integral(hBmdbmul->FindFixBin(0), hBmdbmul->FindFixBin(16));
 Double_t c1 = 0.1;
 Double_t c2 = 0.2;
 Double_t c3 = 0.3;
 Double_t c4 = 0.4;
 Double_t c5 = 0.5;
 Double_t c6 = 0.6;
 Double_t c7 = 0.7;
 Double_t c8 = 0.8;
 Double_t c9 = 0.9;
 Double_t c10 = 1.0;

 Double_t b1 = 100;
 Double_t b2 = 100;
 Double_t b3 = 100;
 Double_t b4 = 100;
 Double_t b5 = 100;
 Double_t b6 = 100;
 Double_t b7 = 100;
 Double_t b8 = 100;
 Double_t b9 = 100;
 Double_t b10 = 100;

Int_t bBin = hBmdbmul->FindFixBin(a);

              while (   TMath::Abs(fraction-c1) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;    
              } 
              b1= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

             while (   TMath::Abs(fraction-c2) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
               b2= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c3) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b3= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c4) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b4= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c5) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b5= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c6) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b6= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c7) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b7= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c8) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b8= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

                while (   TMath::Abs(fraction-c9) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              }
              b9= hBmdbmul->GetXaxis()->GetBinCenter(bBin);
 
                while (   TMath::Abs(fraction-c10) > epsilon  && bBin < hBmdbmul->GetNbinsX()  ) {
               bBin =bBin+1;
               fraction =  hBmdbmul->Integral(hBmdbmul->FindFixBin(a),bBin, "") /totalIntegral;        
              } 
              b10= hBmdbmul->GetXaxis()->GetBinCenter(bBin);

               cout<<"b1: "<<b1<<endl; 
               cout<<"b2: "<<b2<<endl;  
               cout<<"b3: "<<b3<<endl; 
               cout<<"b4: "<<b4<<endl; 
               cout<<"b5: "<<b5<<endl; 
               cout<<"b6: "<<b6<<endl; 
               cout<<"b7: "<<b7<<endl; 
               cout<<"b8: "<<b8<<endl; 
               cout<<"b9: "<<b9<<endl; 
               cout<<"b10: "<<b10<<endl; 
 
               cout<<"fraction: "<<fraction<<endl;  
               cout<<"bBin: "<<bBin<<endl; 
               cout<<"epsilon: "<<epsilon<<endl;   
               cout<<"bin number : "<<hBmdbmul->FindFixBin(bBin)<<endl;  
               cout<<"totalIntegral: "<<totalIntegral<<endl;  

              
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////Multiplicity classes ranges///////////////////////////////////////////////
Int_t classmul1 = 0.1*totalIntegral;
Int_t classmul2 = 0.2*totalIntegral;
Int_t classmul3 = 0.3*totalIntegral;
Int_t classmul4 = 0.4*totalIntegral;
Int_t classmul5 = 0.5*totalIntegral;
Int_t classmul6 = 0.6*totalIntegral;
Int_t classmul7 = 0.7*totalIntegral;
Int_t classmul8 = 0.8*totalIntegral;
Int_t classmul9 = 0.9*totalIntegral;
Int_t classmul10 = totalIntegral;

               cout<<"classmul1: "<<classmul1<<endl; 
               cout<<"classmul2: "<<classmul2<<endl;  
               cout<<"classmul3: "<<classmul3<<endl; 
               cout<<"classmul4: "<<classmul4<<endl; 
               cout<<"classmul5: "<<classmul5<<endl; 
               cout<<"classmul6: "<<classmul6<<endl; 
               cout<<"classmul7: "<<classmul7<<endl; 
               cout<<"classmul8: "<<classmul8<<endl; 
               cout<<"classmul9: "<<classmul9<<endl; 
               cout<<"classmul10: "<<classmul10<<endl; 


//////////////////////////////////////////////////////////////////////////////////////

   //Centrality classes impact parameter//////////////////////////////////
   TCanvas *c12 = new TCanvas("c12","Impact parameter ranges",800,800);
   gStyle->SetOptStat(false);                                   
   //gStyle->SetPalette(1);                                       
   c12->SetRightMargin(0.0465116);
   c12->SetTopMargin(0.1);
   c12->SetFillColor(0);
/*
   hrange1bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange2bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange3bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange4bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange5bbmd->Scale(1.0/hBmdbmul->Integral());
   hrange6bbmd->Scale(1.0/hBmdbmul->Integral());
*/
   hrange1bbmd->Draw();  
   hrange2bbmd->Draw("sames");  
   hrange3bbmd->Draw("sames");  
   hrange4bbmd->Draw("sames");   
   hrange5bbmd->Draw("sames");  
   hrange6bbmd->Draw("sames"); 
   hrange7bbmd->Draw("sames"); 
   hrange8bbmd->Draw("sames"); 
   hrange9bbmd->Draw("sames"); 
   hrange10bbmd->Draw("sames"); 
  
    TLegend *leg = new TLegend(0.65,0.8,0.92,0.89);
   leg->SetTextFont(62);
  // leg->SetTextSize(1);                                    
   leg->SetLineColor(0);
   leg->SetLineStyle(0);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(1001);
   leg->AddEntry("","Impact parameter ranges","");
   leg->AddEntry("hrange1bbmd","class 1","p");
   leg->AddEntry("hrange2bbmd","class 2","p");
   leg->AddEntry("hrange3bbmd","class 3","p");
   leg->AddEntry("hrange4bbmd","class 4","p"); 
   leg->AddEntry("hrange5bbmd","class 5","p");
   leg->AddEntry("hrange6bbmd","class 6","p");
   leg->AddEntry("hrange7bbmd","class 7","p");
   leg->AddEntry("hrange8bbmd","class 8","p");
   leg->AddEntry("hrange9bbmd","class 9","p");
   leg->AddEntry("hrange10bbmd","class 10","p");
   leg->Draw();


   c12->SaveAs("bclasses.pdf");
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////

   /////Multiplicity Centrality classes /////////////////////////////////////////////
   TCanvas *c13 = new TCanvas("c13","Impact parameter ranges",800,800);
   gStyle->SetOptStat(false);                                   
   //gStyle->SetPalette(1);                                       
   c13->SetRightMargin(0.0465116);
   c13->SetTopMargin(0.1);
   c13->SetFillColor(0);

   hmulclass1->Draw();  
   hmulclass2->Draw("sames");  
   hmulclass3->Draw("sames");  
   hmulclass4->Draw("sames");   
   hmulclass5->Draw("sames");  
   hmulclass6->Draw("sames"); 
   hmulclass7->Draw("sames"); 
   hmulclass8->Draw("sames"); 
   hmulclass9->Draw("sames"); 
   hmulclass10->Draw("sames"); 
  
    TLegend *leg2 = new TLegend(0.65,0.8,0.92,0.89);
   leg2->SetTextFont(62);
  // leg2->SetTextSize(1);                                    
   leg2->SetLineColor(0);
   leg2->SetLineStyle(0);
   leg2->SetLineWidth(1);
   leg2->SetFillColor(0);
   leg2->SetFillStyle(1001);
   leg2->AddEntry("","Multiplicity classes","");
   leg2->AddEntry("hmulclass1","class 1","p");
   leg2->AddEntry("hmulclass2","class 2","p");
   leg2->AddEntry("hmulclass3","class 3","p");
   leg2->AddEntry("hmulclass4","class 4","p"); 
   leg2->AddEntry("hmulclass5","class 5","p");
   leg2->AddEntry("hmulclass6","class 6","p");
   leg2->AddEntry("hmulclass7","class 7","p");
   leg2->AddEntry("hmulclass8","class 8","p");
   leg2->AddEntry("hmulclass9","class 9","p");
   leg2->AddEntry("hmulclass9","class 10","p");
   leg2->Draw();


   c13->SaveAs("mulclasses.pdf");
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   

  cout<<"End histograms"<<endl;
  return 0;
   
}

