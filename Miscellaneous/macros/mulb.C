
// Description:
//      
//       Analysis macro. We can study muliplicity distributions and the correlation between 
//       multiplicity and impact parameter, which is important for centrality determination.
//
// Environment:
//      MPDROOT
//
// Author List:
//       Luis Valenzuela-Cazares          (original author)
//   
//-----------------------------------------------------------
#if !defined(__CINT__) && !defined(__CLING__)
#include "TString.h"
#include "TStopwatch.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TMath.h"
#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>

using namespace std;

#endif

#include "/home/luis/mpdroot/macro/mpd/mpdloadlibs.C"

/////////////////////////////////////////
  //Function to get the ring.
   Int_t GetRing(Int_t detID){

  if(detID >= 1 && detID <= 12) return 1;
  else if (detID>=13 && detID<=30) return 2;
  else if (detID>=31 && detID<= 54) return 3;
  else if (detID>=55 && detID<=84) return 4;
  else if (detID>=85 && detID<=120) return 5;
  else if (detID>=121 && detID<=162) return 6;
  
  else if (detID>=163 && detID<=174) return 1;
  else if (detID>=175 && detID<=192) return 2;
  else if (detID>=193 && detID<=216) return 3;
  else if (detID>=217 && detID<=246) return 4;
  else if (detID>=247 && detID<=282) return 5;
  else if (detID>=283 && detID<=324) return 6;

   else return -1;

}
        
Int_t mulb(TString inputFile="/home/luis/Analysis-BEBE/10000AuAu11GeV16fm.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest_AuAu_9GeV_bbc_hex_5cm_NDetScin_0_5000.root")
//Int_t mulb(TString inputFile="/home/luis/Analysis-BEBE/evetest-bmd-5000AuAu9GeV-b0-12.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest_AuAu_9GeV_bbc_hex_5cm_NDetScin_0_5000.root")

{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gStyle->SetOptDate(0);      //show day and time
    gStyle->SetOptStat(1);      //show statistic
    gStyle->SetPalette(1,0);    
      
   // mpdloadlibs(); 
  
 
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Primary charged particles (PCP) multiplicity/////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   ////////////////////////////////Multiplicity per ring BMD A/////////////////////////////////////////////////////// 
  TH1D *hBmdR1Mul = new TH1D("hBmdR1Mul","Primary charged particles multiplicity BMD ring 1. 5000 Au+Au @9GeV UrQMD. MPDROOT.",50,0,50);
  hBmdR1Mul->SetXTitle("Ring");
  hBmdR1Mul->SetYTitle("Multiplicity");

  TH1D *hBmdR2Mul = new TH1D("hBmdR2Mul","Primary charged particles multiplicity BMD ring 2. 5000 Au+Au @9GeV UrQMD. MPDROOT.",50,0,50);
  hBmdR2Mul->SetXTitle("Ring");
  hBmdR2Mul->SetYTitle("Multiplicity");

  TH1D *hBmdR3Mul = new TH1D("hBmdR3Mul","Primary charged particles multiplicity BMD ring 3. 5000 Au+Au @9GeV UrQMD. MPDROOT.",50,0,50);
  hBmdR3Mul->SetXTitle("Ring");
  hBmdR3Mul->SetYTitle("Multiplicity");

  TH1D *hBmdR4Mul = new TH1D("hBmdR4Mul","Primary charged particles multiplicity BMD ring 4. 5000 Au+Au @9GeV UrQMD. MPDROOT.",50,0,50);
  hBmdR4Mul->SetXTitle("Ring");
  hBmdR4Mul->SetYTitle("Multiplicity");

  TH1D *hBmdR5Mul = new TH1D("hBmdR5Mul","Primary charged particles multiplicity BMD ring 5. 5000 Au+Au @9GeV UrQMD. MPDROOT.",50,0,50);
  hBmdR5Mul->SetXTitle("Ring");
  hBmdR5Mul->SetYTitle("Multiplicity");

  TH1D *hBmdR6Mul = new TH1D("hBmdR6Mul","Primary charged particles multiplicity BMD ring 6. 5000 Au+Au @9GeV UrQMD. MPDROOT.",50,0,50);
  hBmdR6Mul->SetXTitle("Ring");
  hBmdR6Mul->SetYTitle("Multiplicity");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////Multiplicity vs Impact parameter/////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////BMD A andC///////////////////////////////////////////////////////////////////////////
   TH1F *hBmdb = new TH1F("hBmdb","Impact parameter",32,0,16);
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////Multiplicity BMD A and BMD C/////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////Multiplicity charged particles////////////////////////////////////////////////////////////// 
    TH1D *hBmdMul = new TH1D("hBmdMul","Charged particles multiplicity BMD A and C. 5000 Au+Au @9GeV UrQMD. MPDROOT.",150,0,150);
   hBmdMul->SetXTitle("BMD A and C");  
   
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //TH2F *hBMDbvsMul = new TH2F("hBMDbvsMul","Multiplicity vs impact parameter",15,0,15,100,0,100);

 //TProfile *hBMDbvsMul  = new TProfile("hBMDbvsMul","Mean multiplicity versus impact parameter.",16,0,16,0,320);
  TProfile *hBMDbvsMul  = new TProfile("hBMDbvsMul","Mean multiplicity versus impact parameter.",14,0,14,0,280);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//TProfile histograms of multiplicity of each bmd A ring vs impact parameter.
 TProfile *hprofmulbmdring1  = new TProfile("hprofmulbmdring1","Mean multiplicity of BMD ring 1 versus impact parameter. 5000 Au+Au @9GeV UrQMD. MPDROOT.",16,0,16,0,320);
 TProfile *hprofmulbmdring2  = new TProfile("hprofmulbmdring2","Mean multiplicity of BMD ring 2 versus impact parameter. 5000 Au+Au @9GeV UrQMD. MPDROOT.",16,0,16,0,320);
 TProfile *hprofmulbmdring3  = new TProfile("hprofmulbmdring3","Mean multiplicity of BMD ring 3 versus impact parameter. 5000 Au+Au @9GeV UrQMD. MPDROOT.",16,0,16,0,320);
 TProfile *hprofmulbmdring4  = new TProfile("hprofmulbmdring4","Mean multiplicity of BMD ring 4 versus impact parameter. 5000 Au+Au @9GeV UrQMD. MPDROOT.",16,0,16,0,320);
 TProfile *hprofmulbmdring5  = new TProfile("hprofmulbmdring5","Mean multiplicity of BMD ring 5 versus impact parameter. 5000 Au+Au @9GeV UrQMD. MPDROOT.",16,0,16,0,320);
 TProfile *hprofmulbmdring6  = new TProfile("hprofmulbmdring6","Mean multiplicity of BMD ring 5 versus impact parameter. 5000 Au+Au @9GeV UrQMD. MPDROOT.",16,0,16,0,320);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Defining pointers to data
    
    TFile fileInput(inputFile.Data());
    
    TTree *simEvent = (TTree*) fileInput.Get("cbmsim");
    
    //Arrays/////nombre de la variable///////////nombre en la memoria interna
    TClonesArray *bmdPoints = (TClonesArray*) fileInput.FindObjectAny("BmdPoint");
    simEvent->SetBranchAddress("BmdPoint",&bmdPoints);
    
 //   TClonesArray *bbcPoints = (TClonesArray*) fileInput.FindObjectAny("BBCPoint");
 //    simEvent->SetBranchAddress("BBCPoint",&bbcPoints);

    TClonesArray* mcTracks = (TClonesArray*) fileInput.FindObjectAny("MCTrack");
    simEvent->SetBranchAddress("MCTrack",&mcTracks);
  
 //  TClonesArray* tpcPoints = (TClonesArray*) fileInput.FindObjectAny("TpcPoint");
 //   simEvent->SetBranchAddress("TpcPoint",&tpcPoints);
  
    FairMCEventHeader* mcHeader = (FairMCEventHeader*) fileInput.FindObjectAny("MCEventHeader.");
    simEvent->SetBranchAddress("MCEventHeader.",&mcHeader);
        
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//MCTrack kinematics
//MCEventHeader kind of collision  

    //Declaring variable for the impact parameter.
    Double_t impactParameter; //! impact parameter 

     TFile* fileOutput = new TFile(Form("%s",outputFile.Data()),"RECREATE");

     //Defining the tree and a branch for the impact parameter.
     TTree *bmdTree = new TTree("bmdTree","BMD");
     bmdTree->Branch("impactParameter",&impactParameter);
    
     Int_t events  = simEvent->GetEntries();
  
    //Begins loop for the events
    for (Int_t i = 0000; i < events; ++i) {
    simEvent->GetEntry(i);

     MpdMCEventHeader *extraEventHeader = dynamic_cast<MpdMCEventHeader*> (mcHeader);

      impactParameter = extraEventHeader->GetB();
      
      bmdTree->Fill();
      hBmdb->Fill(impactParameter);

//    cout<<"Impact parameter: "<<extraEventHeader->GetB()<<endl;
   
    Int_t nMCTracks  =  mcTracks->GetEntriesFast();
    Int_t nprimary   =  mcHeader->GetNPrim(); 
  //  cout<<"nMCTracks "<<nMCTracks<<endl;

    Double_t B  = mcHeader->GetB();
  
    Int_t nChargedMultiplicityBMD=0;
 
    Int_t nMultiplicityPerRing[7]={0,0,0,0,0,0,0};  //Arrays for primary charged particles 


    if (bmdPoints != 0 ) {

    Int_t nbmdPoints =  bmdPoints->GetEntriesFast();
    if( nbmdPoints < 1 ) continue;
    
 /*     
    if (bbcPoints != 0 ) {
    Int_t nbbcPoints =  bbcPoints->GetEntriesFast();
    if( nbbcPoints < 1 ) continue;
 */     
    
    //cout<<"nbmdPoints: "<<nbmdPoints<<endl;
       
    //Loop for particles in both BMD.
   
      for (Int_t j = 0; j < nbmdPoints; j++) {
      BmdPoint *p1    = (BmdPoint*) bmdPoints->At(j);
   //   for (Int_t j = 0; j < nbbcPoints; j++) {
   //   MpdBbcPoint *p1    = (MpdBbcPoint*) bbcPoints->At(j);
      TVector3 recMom;
      Int_t    trkID = p1->GetTrackID();
      Double_t time  = p1->GetTime();
      Double_t eLoss = p1->GetEnergyLoss();
      p1->Momentum(recMom);
      Double_t currPt  = recMom.Pt();
      Double_t currEta = recMom.Eta();
      
      Int_t detID =  p1->GetDetectorID();
     // Int_t statusCode = p1->GetStatusCode();
 
    //    cout<<"trkID: "<<trkID<<endl;  
    //    cout<<"detID: "<<detID<<endl;            

     Int_t x = p1->GetX();
     Int_t y = p1->GetY();
     Int_t z = p1->GetZ();

   //  cout<<"x: "<<x<<"y: "<<y<<"z: "<<z<<endl;   

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////Analysis per ring////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Int_t ring = -1;
  
ring = GetRing(detID);
 //cout<<"ring: "<<ring<<endl;


        FairMCTrack* mcTr = (FairMCTrack*) mcTracks->UncheckedAt(trkID);

          Double_t Xv = mcTr->GetStartX();
          Double_t Yv = mcTr->GetStartY();
          Double_t Zv = mcTr->GetStartZ();
      
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////Primary Particles Condition////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////
  if( mcTr->GetMotherId() < 0 ) {

   //Charged primary particles
         if ( TMath::Abs(mcTr->GetPdgCode() )  != 211  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 11   &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 2212 && //Protons
                  TMath::Abs(mcTr->GetPdgCode() )  != 321  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 13   &&
                  (TMath::Abs(Xv) > 0.001 || TMath::Abs(Yv) > 0.001 || TMath::Abs(Zv) > 0.01 )) continue;

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////Primary Charged Particles BMD A/////////////////////////
////////////////////////////////////////////////////////////////////////////////////////   

/////////////////////////////////Rings////////////////////////////////////////////        
//if(z >=199){ //BMD A    
//if (z <=-199){       //BMD C
   //if( TMath::Abs(recMom.Eta()) >  3.5 && TMath::Abs(recMom.Eta()) <= 4.2 ) {
    if( ring==1){  //Ring 1     
             // cout<<"z: "<<z<<endl;        
                  nMultiplicityPerRing[1]++;
           //       nChargedMultiplicityBMD++;                  
               
     }    

  // if( TMath::Abs(recMom.Eta()) >  3.2 && TMath::Abs(recMom.Eta()) <= 3.5 ) {                                 
    if( ring==2){ //Ring 2
              //  cout<<"z: "<<z<<endl;       
                 nMultiplicityPerRing[2]++;
               //  nChargedMultiplicityBMD++;
     }    

 //if( TMath::Abs(recMom.Eta()) >  3.0 && TMath::Abs(recMom.Eta()) <= 3.2 ) {      
    if( ring==3){  //Ring 3
                 nMultiplicityPerRing[3]++;
                 nChargedMultiplicityBMD++;
     }    
 
 //if( TMath::Abs(recMom.Eta()) >  2.8 && TMath::Abs(recMom.Eta()) <= 3.0 ) {                      
    if( ring==4){  //Ring 4 
           nMultiplicityPerRing[4]++;
           nChargedMultiplicityBMD++; 
     }    
  
//if( TMath::Abs(recMom.Eta()) >  2.6 && TMath::Abs(recMom.Eta()) <= 2.8 ) {                        
    if( ring==5){   //Ring 5    
              nMultiplicityPerRing[5]++;
              nChargedMultiplicityBMD++;
    }    

 //if( TMath::Abs(recMom.Eta()) >=  2.2 && TMath::Abs(recMom.Eta()) <= 2.6 ) {        
    if( ring==6){ //Ring 6
               nMultiplicityPerRing[6]++;
               nChargedMultiplicityBMD++;
     }    

  // } //BMD A or C

///////////////////////////////////////////////////////////////////////////////////////////
    
}


}  //Primary Particles Condition 

}

    if (mcTracks != 0) {
  //  cout<<"AntesMC"<<endl;

////////////////////////////////
    
      Int_t nMCtracks = mcTracks->GetEntriesFast();

     for(Int_t iPar = 0; iPar < nMCtracks; iPar++)
     {
       FairMCTrack* currMCtrk = ( FairMCTrack*)mcTracks->UncheckedAt(iPar);
       TLorentzVector momMC(currMCtrk->GetPx(),currMCtrk->GetPy(),currMCtrk->GetPz(),currMCtrk->GetEnergy());
       
       Int_t pdgCode = currMCtrk->GetPdgCode();       

    //  bmdTree->Fill();

     } //! loop over MC tracks
    ///////////////////////////////   
    
  } //End if mctrack 

 
///////////////////////////Multiplicity charged particles//////////////////////////////////////////////////////////
     hBmdMul->Fill(nChargedMultiplicityBMD);
     hBMDbvsMul->Fill(impactParameter,nChargedMultiplicityBMD);

  // cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<"impactParameter:"<<impactParameter<<endl;
  //  cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<endl;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////BMDA Rings multiplicity vs impact parameter/////////////////////////////////////////////////////
              hprofmulbmdring1->Fill(impactParameter,nMultiplicityPerRing[1]);
              hprofmulbmdring2->Fill(impactParameter,nMultiplicityPerRing[2]);
              hprofmulbmdring3->Fill(impactParameter,nMultiplicityPerRing[3]);
              hprofmulbmdring4->Fill(impactParameter,nMultiplicityPerRing[4]);
              hprofmulbmdring5->Fill(impactParameter,nMultiplicityPerRing[5]);
              hprofmulbmdring6->Fill(impactParameter,nMultiplicityPerRing[6]);
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////Filling Rings multiplicity primary charged particles/////////////////////// 
              hBmdR1Mul->Fill(nMultiplicityPerRing[1]);
              hBmdR2Mul->Fill(nMultiplicityPerRing[2]);
              hBmdR3Mul->Fill(nMultiplicityPerRing[3]);
              hBmdR4Mul->Fill(nMultiplicityPerRing[4]);
              hBmdR5Mul->Fill(nMultiplicityPerRing[5]);
              hBmdR6Mul->Fill(nMultiplicityPerRing[6]);

/////////////////////////////////////////////////////////////////////////////////////////

  } //End of the loop's event. 
  
 ///////////////////////////////////////// 

  bmdTree->Write();
  
///////////////////////////////////////////////  
   cout<<"Saving histograms"<<endl;
  
   
   fileOutput->mkdir("Mc");
   fileOutput->cd("Mc");
   
  gStyle->SetOptTitle(0); //No title for histograms


/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Impact parameter analysis//////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

TCanvas *c50 = new TCanvas("c50","Impact parameter (fm)");
hBmdb->SetStats(kFALSE);
hBmdb->GetXaxis()->SetTitle("b(fm)");
hBmdb->GetYaxis()->SetTitle("events number");
//hBmdb->Scale(1./hBmdb->Integral());
hBmdb->Draw();

TCanvas *c51 = new TCanvas("c51","Charged particles multiplicity vs impact parameter BMD ");
hBMDbvsMul->SetStats(kFALSE);
hBMDbvsMul->GetXaxis()->SetTitle("b(fm)");
hBMDbvsMul->GetYaxis()->SetTitle("Multiplicity");
//hBMDbvsMul->Scale(1./hBMDbvsMul->Integral());
hBMDbvsMul->Draw();

TCanvas *c54 = new TCanvas("c54","Charged particles multiplicity BMD A and C");
hBmdMul->SetStats(kFALSE);
hBmdMul->GetXaxis()->SetTitle("Multiplicity");
//hBmdMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hBmdMul->GetYaxis()->SetTitle("Events number");
//hBmdMul->Sumw2();
//hBmdMul->Rebin();
//hBmdMul->Scale(1./hBmdMul->Integral());
hBmdMul->Draw();

//Int_t hmulIntegral = hBmdMul->Integral();
//cout<<"hmulIntegral: "<<hmulIntegral<<endl;  


 ////////////////////////Primary charged particles multiplicity BMD///////////////////
  
  ///////////////////////////BMD A///////////////////////////////////////////////////// 
TCanvas *c47 = new TCanvas("c47","Primary charged particles multiplicity per ring BMD");
   c47->Divide(3,2);

   c47->cd(1); 
 hBmdR1Mul->SetStats(kFALSE);
 hBmdR1Mul->GetXaxis()->SetTitle("Multiplicity");
 hBmdR1Mul->GetYaxis()->SetTitle("Events number");
 hBmdR1Mul->Draw();

   c47->cd(2); 
 hBmdR2Mul->SetStats(kFALSE);
 hBmdR2Mul->GetXaxis()->SetTitle("Multiplicity");
 hBmdR2Mul->GetYaxis()->SetTitle("Events number");
 hBmdR2Mul->Draw();

   c47->cd(3); 
 hBmdR3Mul->SetStats(kFALSE);
 hBmdR3Mul->GetXaxis()->SetTitle("Multiplicity");
 hBmdR3Mul->GetYaxis()->SetTitle("Events number");
 hBmdR3Mul->Draw();

   c47->cd(4); 
 hBmdR4Mul->SetStats(kFALSE);
 hBmdR4Mul->GetXaxis()->SetTitle("Multiplicity");
 hBmdR4Mul->GetYaxis()->SetTitle("Events number");
 hBmdR4Mul->Draw();

 c47->cd(5); 
 hBmdR5Mul->SetStats(kFALSE);
 hBmdR5Mul->GetXaxis()->SetTitle("Multiplicity");
 hBmdR5Mul->GetYaxis()->SetTitle("Events number");
 hBmdR5Mul->Draw();

 c47->cd(6); 
 hBmdR6Mul->SetStats(kFALSE);
 hBmdR6Mul->GetXaxis()->SetTitle("Multiplicity");
 hBmdR6Mul->GetYaxis()->SetTitle("Events number");
 hBmdR6Mul->Draw();

////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////Rings multiplicity vs impact parameter//////////////////////////////////////////////////////////
   TCanvas *cprofile1 = new TCanvas("cprofile1","BMD Rings primary charged particles multiplicity vs impact parameter",200,10,700,500);
   cprofile1->Divide(3,3);
   //cprofile1->Divide(2,3,0,0);
   //TCanvas->Divide(2,3,0,0);
   cprofile1->cd(1);
   hprofmulbmdring1->SetStats(kFALSE);
   hprofmulbmdring1->GetXaxis()->SetTitle("b(fm)");
   hprofmulbmdring1->GetYaxis()->SetTitle("Mean multiplicity ring 1 ");
   hprofmulbmdring1->GetYaxis()->SetTitleOffset(0.55);
   hprofmulbmdring1->GetXaxis()->SetTitleSize(0.05);
   hprofmulbmdring1->GetYaxis()->SetTitleSize(0.05);
   hprofmulbmdring1->Draw();

   cprofile1->cd(2);
   hprofmulbmdring2->SetStats(kFALSE);
   hprofmulbmdring2->GetXaxis()->SetTitle("b(fm)");
   hprofmulbmdring2->GetYaxis()->SetTitle("Mean multiplicity ring 2 ");
   hprofmulbmdring2->GetYaxis()->SetTitleOffset(0.55);
   hprofmulbmdring2->GetXaxis()->SetTitleSize(0.05);
   hprofmulbmdring2->GetYaxis()->SetTitleSize(0.05);
   hprofmulbmdring2->Draw();

   cprofile1->cd(3);
   hprofmulbmdring3->SetStats(kFALSE);
   hprofmulbmdring3->GetXaxis()->SetTitle("b(fm)");
   hprofmulbmdring3->GetYaxis()->SetTitle("Mean multiplicity ring 3 ");
    hprofmulbmdring3->GetYaxis()->SetTitleOffset(0.55);
   hprofmulbmdring3->GetXaxis()->SetTitleSize(0.05);
   hprofmulbmdring3->GetYaxis()->SetTitleSize(0.05);
   hprofmulbmdring3->Draw();

   cprofile1->cd(4);
   hprofmulbmdring4->SetStats(kFALSE);
   hprofmulbmdring4->GetXaxis()->SetTitle("b(fm)");
   hprofmulbmdring4->GetYaxis()->SetTitle("Mean multiplicity ring 4 ");
    hprofmulbmdring4->GetYaxis()->SetTitleOffset(0.55);
   hprofmulbmdring4->GetXaxis()->SetTitleSize(0.05);
   hprofmulbmdring4->GetYaxis()->SetTitleSize(0.05);
   hprofmulbmdring4->Draw();
   
   cprofile1->cd(5);
   hprofmulbmdring5->SetStats(kFALSE);
   hprofmulbmdring5->GetXaxis()->SetTitle("b(fm)");
   hprofmulbmdring5->GetYaxis()->SetTitle("Mean multiplicity ring 5 ");
    hprofmulbmdring5->GetYaxis()->SetTitleOffset(0.55);
   hprofmulbmdring5->GetXaxis()->SetTitleSize(0.05);
   hprofmulbmdring5->GetYaxis()->SetTitleSize(0.05);
   hprofmulbmdring5->Draw();

   cprofile1->cd(6);
   hprofmulbmdring6->SetStats(kFALSE);
   hprofmulbmdring6->GetXaxis()->SetTitle("b(fm)");
   hprofmulbmdring6->GetYaxis()->SetTitle("Mean multiplicity ring 6 ");
    hprofmulbmdring6->GetYaxis()->SetTitleOffset(0.55);
   hprofmulbmdring6->GetXaxis()->SetTitleSize(0.05);
   hprofmulbmdring6->GetYaxis()->SetTitleSize(0.05);
   hprofmulbmdring6->Draw();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  cout<<"End histograms"<<endl;
  return 0;
   
}

