
// Description:
//      
//       Analysis macro. This macro determines the ranges of multiplicity for each centrality class.
//
// Environment:
//      MPDROOT
//
// Author List:
//       Luis Valenzuela-Cazares          (original author)
//   
//-----------------------------------------------------------
#if !defined(__CINT__) && !defined(__CLING__)
#include "TString.h"
#include "TStopwatch.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TMath.h"
#include "MpdGetNumEvents.h"

#include <iostream>
#include <fstream>
#include <TStyle.h>
#include <TCanvas.h>

#include <stdio.h>      /* printf */
#include <math.h>       /* tgamma */

using namespace std;

#endif

#include "/home/luis/mpdroot/macro/mpd/mpdloadlibs.C"

////////////////////////////////////////////////////////////////

/////////////////////////////////////////
  //Function to get the ring.
   Int_t GetRing(Int_t detID){

  if(detID >= 1 && detID <= 12) return 1;
  else if (detID>=13 && detID<=30) return 2;
  else if (detID>=31 && detID<= 54) return 3;
  else if (detID>=55 && detID<=84) return 4;
  else if (detID>=85 && detID<=120) return 5;
  else if (detID>=121 && detID<=162) return 6;
  
  else if (detID>=163 && detID<=174) return 1;
  else if (detID>=175 && detID<=192) return 2;
  else if (detID>=193 && detID<=216) return 3;
  else if (detID>=217 && detID<=246) return 4;
  else if (detID>=247 && detID<=282) return 5;
  else if (detID>=283 && detID<=324) return 6;

   else return -1;

}

Int_t multiplicityclasses(TString inputFile="/home/luis/Analysis-BEBE/10000AuAu11GeV16fm.root",TString outputFile="/home/luis/Analysis-BEBE/salida-evetest.root")
{
    gROOT->Reset();
    gROOT->SetStyle("Plain");
    gStyle->SetOptDate(0);      //show day and time
    gStyle->SetOptStat(1);      //show statistic
    gStyle->SetPalette(1,0);
    
   // mpdloadlibs(); 

///////////////////////////////////////////////Multiplicity charged particles///////////////////////////////////////////////// 
    TH1D *hBmdMul = new TH1D("hBmdMul","Charged particles multiplicity at BEBE.",50,0,50);
    hBmdMul->SetXTitle("BMD A and C");  

     TH1D *hBmdNoMul = new TH1D("hBmdNoMul","Charged particles multiplicity at BEBE.",50,0,50);
    hBmdNoMul->SetXTitle("Multiplicity");  

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    TH1D *hMCTrackMul = new TH1D("hMCTrackMul","Charged particles multiplicity at BEBE.",150,0,150);
    hMCTrackMul->SetXTitle("Multiplicity");  
           
/////////////////////////////////////Multiplicity classes.///////////////////////////////////////////////////////////changenas de 
 TH1F *hmulclass1  = new TH1F("hmulclass1","Multiplicity class 1, MPDROOT at BEBE.",100,0,100);
       hmulclass1->SetXTitle("multiplicity N");
       hmulclass1->SetYTitle("events");
       hmulclass1->SetLineColorAlpha(kBlack, 0.35);
       hmulclass1->GetXaxis()->CenterTitle(true);
       hmulclass1->GetXaxis()->SetTitleSize(0.04);
       hmulclass1->GetXaxis()->SetLabelSize(0.03);
       hmulclass1->GetXaxis()->SetTitleOffset(1.4);
       hmulclass1->SetFillColor(kViolet);

 TH1F *hmulclass2  = new TH1F("hmulclass2","Multiplicity class 2. MPDROOT.",100,0,100);
       hmulclass2->SetLineColorAlpha(kBlack, 0.35);
       hmulclass2->SetFillColor(kBlue);

 TH1F *hmulclass3  = new TH1F("hmulclass3","Multiplicity class 3. MPDROOT.",100,0,100);
       hmulclass3->SetLineColorAlpha(kBlack, 0.35);
       hmulclass3->SetFillColor(kGreen);

 TH1F *hmulclass4  = new TH1F("hmulclass4","Multiplicity class 4. MPDROOT.",100,0,100);
       hmulclass4->SetLineColorAlpha(kBlack, 0.35);
       hmulclass4->SetFillColor(kGray);

 TH1F *hmulclass5  = new TH1F("hmulclass5","Multiplicity class 5. MPDROOT.",100,0,100);
       hmulclass5->SetLineColorAlpha(kBlack, 0.35);
       hmulclass5->SetFillColor(kYellow);

 TH1F *hmulclass6  = new TH1F("hmulclass6","Multiplicity class 6. MPDROOT.",100,0,100);
       hmulclass6->SetLineColorAlpha(kBlack, 0.35);
       hmulclass6->SetFillColor(kGray+50);

 TH1F *hmulclass7  = new TH1F("hmulclass7","Multiplicity class 7. MPDROOT.",100,0,100);
       hmulclass7->SetLineColorAlpha(kBlack, 0.35);
       hmulclass7->SetFillColor(kRed);

  TH1F *hmulclass8  = new TH1F("hmulclass8","Multiplicity class 8. MPDROOT.",100,0,100);
        hmulclass8->SetLineColorAlpha(kBlack, 0.35);
        hmulclass8->SetFillColor(kTeal);

  TH1F *hmulclass9  = new TH1F("hmulclass9","Multiplicity class 9. MPDROOT.",100,0,100);
        hmulclass9->SetLineColorAlpha(kBlack, 0.35);
        hmulclass9->SetFillColor(kBlack);
 
 TH1F *hmulclass10  = new TH1F("hmulclass10","Multiplicity class 10. MPDROOT.",100,0,100);
       hmulclass10->SetLineColorAlpha(kBlack, 0.35);
       hmulclass10->SetFillColor(kOrange);

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
 
    //Defining pointers to data
    
    TFile fileInput(inputFile.Data());
    
    TTree *simEvent = (TTree*) fileInput.Get("cbmsim");
    
    //Arrays/////name variable///////////intern memory name
    TClonesArray *bmdPoints = (TClonesArray*) fileInput.FindObjectAny("BmdPoint");   //Turn on for bmd
    simEvent->SetBranchAddress("BmdPoint",&bmdPoints);
    
    TClonesArray* mcTracks = (TClonesArray*) fileInput.FindObjectAny("MCTrack");
    simEvent->SetBranchAddress("MCTrack",&mcTracks);
   
    FairMCEventHeader* mcHeader = (FairMCEventHeader*) fileInput.FindObjectAny("MCEventHeader.")
;    simEvent->SetBranchAddress("MCEventHeader.",&mcHeader);
        
///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Declaring variable for the impact parameter.
    Double_t impactParameter; //! impact parameter 

     TFile* fileOutput = new TFile(Form("%s",outputFile.Data()),"RECREATE");
   
     Int_t events  = simEvent->GetEntries();
  
    //Begins loop for the events
    for (Int_t i = 0000; i < events; ++i) {
    simEvent->GetEntry(i);

     MpdMCEventHeader *extraEventHeader = dynamic_cast<MpdMCEventHeader*> (mcHeader);

     impactParameter = extraEventHeader->GetB();
   
    Int_t nMCTracks  =  mcTracks->GetEntriesFast();
    Int_t nprimary   =  mcHeader->GetNPrim(); 
  //  cout<<"nMCTracks "<<nMCTracks<<endl;

    Double_t B  = mcHeader->GetB();
  
    Int_t nChargedMultiplicityBMD=0;
    Int_t nChargedMul=0;

  Int_t mulrange1=0;
  Int_t mulrange2=0;
  Int_t mulrange3=0;
  Int_t mulrange4=0;
  Int_t mulrange5=0;
  Int_t mulrange6=0;
  Int_t mulrange7=0;
  Int_t mulrange8=0;
  Int_t mulrange9=0;
  Int_t mulrange10=0;

    if (bmdPoints != 0 ) {

    Int_t nbmdPoints =  bmdPoints->GetEntriesFast();
    if( nbmdPoints < 1 ) continue;
          
    //cout<<"nbmdPoints: "<<nbmdPoints<<endl;
       
    //Loop for particles in both BMD.
   
      for (Int_t j = 0; j < nbmdPoints; j++) {
      BmdPoint *p1    = (BmdPoint*) bmdPoints->At(j);
   //   for (Int_t j = 0; j < nbbcPoints; j++) {
   //   MpdBbcPoint *p1    = (MpdBbcPoint*) bbcPoints->At(j);
      TVector3 recMom;
      Int_t    trkID = p1->GetTrackID();
      Int_t detID =  p1->GetDetectorID();
    //    cout<<"detID: "<<detID<<endl;            

     Int_t x = p1->GetX();
     Int_t y = p1->GetY();
     Int_t z = p1->GetZ();

   //  cout<<"x: "<<x<<"y: "<<y<<"z: "<<z<<endl;   

        FairMCTrack* mcTr = (FairMCTrack*) mcTracks->UncheckedAt(trkID);

          Double_t Xv = mcTr->GetStartX();
          Double_t Yv = mcTr->GetStartY();
          Double_t Zv = mcTr->GetStartZ();

   Int_t ring = -1;
   ring = GetRing(detID);

////////////////////Primary Particles Condition////////////////////////////////////////////////// 
  if( mcTr->GetMotherId() < 0 ) {

   //Charged primary particles
         if ( TMath::Abs(mcTr->GetPdgCode() )  != 211  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 11   &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 2212 && //Protons
                  TMath::Abs(mcTr->GetPdgCode() )  != 321  &&
                  TMath::Abs(mcTr->GetPdgCode() )  != 13   &&
                  (TMath::Abs(Xv) > 0.001 || TMath::Abs(Yv) > 0.001 || TMath::Abs(Zv) > 0.01 )) continue;                    
                   
                  //  if(ring==2 ||ring==3 || ring==4 || ring==5 || ring ==6 ){  
                   if(ring==3 || ring==4 || ring==5 || ring ==6 ){ 
               //  if( ring==4 || ring==5 || ring ==6 ){  
               //     if(ring==5 || ring ==6 ){  

               //  cout<<"ring: "<<ring<<endl;


                            nChargedMultiplicityBMD++; 
               
                    if (nChargedMultiplicityBMD <45)  {        

                            hBmdNoMul->Fill(nChargedMultiplicityBMD);

                    }

                //  nChargedMul++;
 
              } //Rings  
   }


}  //Primary Particles Condition 

}

//Multiplicity charged particles

  if (nChargedMultiplicityBMD <45)  {   

     hBmdMul->Fill(nChargedMultiplicityBMD);  

   }

    // hMCTrackMul->Fill(nChargedMul);
   //  cout<<"nChargedMultiplicityBMD: "<<nChargedMultiplicityBMD<<endl;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////Multiplicity centrality classes///////////////////////////////////////////

//First range
if (0<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<3) {
     mulrange1++;
     hmulclass1->Fill(nChargedMultiplicityBMD);
              }
//Second range
if (3<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<5) {
  mulrange2++;
  hmulclass2->Fill(nChargedMultiplicityBMD);
              }
//Third range
if (5<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<8) {
  mulrange3++;
  hmulclass3->Fill(nChargedMultiplicityBMD);
              }
//Forth range
if (8<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<11) {
  mulrange4++;
  hmulclass4->Fill(nChargedMultiplicityBMD);
              }
//Fifth range
if (11<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<14) {
  mulrange5++;
  hmulclass5->Fill(nChargedMultiplicityBMD);
              }
//Sixth range
if (14<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<18) {
  mulrange6++;
  hmulclass6->Fill(nChargedMultiplicityBMD);
              }
 //Seventh range
if (18<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<23) {
  mulrange7++;
  hmulclass7->Fill(nChargedMultiplicityBMD);
              }
//Eight range
if (23<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<29) {
  mulrange8++;
  hmulclass8->Fill(nChargedMultiplicityBMD);
              }
//Nineth range
if (29<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<37) {
  mulrange9++;
  hmulclass9->Fill(nChargedMultiplicityBMD);
              }
//Tenth range45
if (37<=nChargedMultiplicityBMD && nChargedMultiplicityBMD<100) {
  mulrange10++;
  hmulclass10->Fill(nChargedMultiplicityBMD);
              }   
  
  } //End of the loop's event. 
  
///////////////////////////////////////////////  
   cout<<"Saving histograms"<<endl;
     
   fileOutput->mkdir("Mc");
   fileOutput->cd("Mc");
   
  gStyle->SetOptTitle(0); //No title for histograms


/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////Multiplicity analysis//////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

TCanvas *c54 = new TCanvas("c54","Charged particles multiplicity BMD ");
hBmdMul->SetStats(kFALSE);
hBmdMul->GetXaxis()->SetTitle("Multiplicity");
//hBmdMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hBmdMul->GetYaxis()->SetTitle("Events number");
//hBmdMul->Sumw2();
//hBmdMul->Rebin();
//hBmdMul->Scale(1./hBmdMul->Integral());
hBmdMul->Draw();

TCanvas *c56 = new TCanvas("c56","Charged particles multiplicity BMD ");
hBmdNoMul->SetStats(kFALSE);
hBmdNoMul->GetXaxis()->SetTitle("Multiplicity");
//hBmdNoMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hBmdNoMul->GetYaxis()->SetTitle("Number of times");
hBmdNoMul->Draw();

/*
TCanvas *c55 = new TCanvas("c55","Charged particles multiplicity MCTrack ");
hMCTrackMul->SetStats(kFALSE);
hMCTrackMul->GetXaxis()->SetTitle("Multiplicity");
//hMCTrackMul->GetYaxis()->SetTitle("1/N dN{evt}/dN_{ch}");
hMCTrackMul->GetYaxis()->SetTitle("Events number");
//hMCTrackMul->Sumw2();
//hMCTrackMul->Rebin();
//hMCTrackMul->Scale(1./hMCTrackMul->Integral());
hMCTrackMul->Draw();
*/
 
//////////////////////////////////////////Multplicity classes//////////////////////////////////////////////////////////
 Double_t am = 5;
 Double_t fractionmul =100;
  Double_t epsilonmul = 0.1; 

// Int_t epsilonmul = 0.018; 
 Int_t totalIntegralMul = hBmdNoMul->Integral(hBmdNoMul->FindFixBin(0), hBmdNoMul->FindFixBin(100));
 Double_t cm1 = 0.1;
 Double_t cm2 = 0.2;
 Double_t cm3 = 0.3;
 Double_t cm4 = 0.4;
 Double_t cm5 = 0.5;
 Double_t cm6 = 0.6;
 Double_t cm7 = 0.7;
 Double_t cm8 = 0.8;
 Double_t cm9 = 0.9;
 Double_t cm10 = 1.0;

 Int_t m1 = 100;
 Int_t m2 = 100;
 Int_t m3 = 100;
 Int_t m4 = 100;
 Int_t m5 = 100;
 Int_t m6 = 100;
 Int_t m7 = 100;
 Int_t m8 = 100;
 Int_t m9 = 100;
 Int_t m10 = 100;

Int_t mBin = hBmdNoMul->FindFixBin(am);

              while (   TMath::Abs(fractionmul-cm1) > epsilonmul && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;    
              } 
              m1= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

             while (   TMath::Abs(fractionmul-cm2) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul = hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
               m2= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm3) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m3= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm4) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m4= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm5) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m5= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm6) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m6= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm7) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m7= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm8) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m8= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

                while (   TMath::Abs(fractionmul-cm9) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              }
              m9= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);
 
                while (   TMath::Abs(fractionmul-cm10) > epsilonmul  && mBin < hBmdNoMul->GetNbinsX()  ) {
               mBin =mBin+1;
               fractionmul =  hBmdNoMul->Integral(hBmdNoMul->FindFixBin(am),mBin, "") /totalIntegralMul;        
              } 
              m10= hBmdNoMul->GetXaxis()->GetBinCenter(mBin);

               cout<<"m1: "<<m1<<endl; 
               cout<<"m2: "<<m2<<endl;  
               cout<<"m3: "<<m3<<endl; 
               cout<<"m4: "<<m4<<endl; 
               cout<<"m5: "<<m5<<endl; 
               cout<<"m6: "<<m6<<endl; 
               cout<<"m7: "<<m7<<endl; 
               cout<<"m8: "<<m8<<endl; 
               cout<<"m9: "<<m9<<endl; 
               cout<<"m10: "<<m10<<endl; 
               cout<<""""""""""<<endl;        
 
               cout<<"fractionmul: "<<fractionmul<<endl;  
               cout<<"mBin: "<<mBin<<endl; 
               cout<<"epsilonmul: "<<epsilonmul<<endl;   
               cout<<"bin number : "<<hBmdNoMul->FindFixBin(mBin)<<endl;  
               cout<<"totalIntegralMul: "<<totalIntegralMul<<endl;  

///////////////////////////////////////////////////Multiplicity classes ranges///////////////////////////////////////////////
Int_t cmul1 = 0.1*totalIntegralMul;
Int_t cmul2 = 0.2*totalIntegralMul;
Int_t cmul3 = 0.3*totalIntegralMul;
Int_t cmul4 = 0.4*totalIntegralMul;
Int_t cmul5 = 0.5*totalIntegralMul;
Int_t cmul6 = 0.6*totalIntegralMul;
Int_t cmul7 = 0.7*totalIntegralMul;
Int_t cmul8 = 0.8*totalIntegralMul;
Int_t cmul9 = 0.9*totalIntegralMul;
Int_t cmul10 = totalIntegralMul;

               cout<<"cmul1: "<<cmul1<<endl; 
               cout<<"cmul2: "<<cmul2<<endl;  
               cout<<"cmul3: "<<cmul3<<endl; 
               cout<<"cmul4: "<<cmul4<<endl; 
               cout<<"cmul5: "<<cmul5<<endl; 
               cout<<"cmul6: "<<cmul6<<endl; 
               cout<<"cmul7: "<<cmul7<<endl; 
               cout<<"cmul8: "<<cmul8<<endl; 
               cout<<"cmul9: "<<cmul9<<endl; 
               cout<<"cmul10: "<<cmul10<<endl;  
               cout<<""""""""""<<endl;        

 ////////Checking multiplicity classes///////////////////////////////

               //Cut at mul 45
 /* 
 Int_t class1 = hBmdNoMul->Integral(0,3);
 Int_t class2 = hBmdNoMul->Integral(0,5);
 Int_t class3 = hBmdNoMul->Integral(0,8);
 Int_t class4 = hBmdNoMul->Integral(0,11);
 Int_t class5 = hBmdNoMul->Integral(0,14);
 Int_t class6 = hBmdNoMul->Integral(0,17);
 Int_t class7 = hBmdNoMul->Integral(0,22);
 Int_t class8 = hBmdNoMul->Integral(0,26);
 Int_t class9 = hBmdNoMul->Integral(0,33);
 Int_t class10 = hBmdNoMul->Integral(0,45);
*/
  
  //Rings 3, 4, 5, 6, no cuts.           
 Int_t class1 = hBmdNoMul->Integral(0,3);
 Int_t class2 = hBmdNoMul->Integral(0,5);
 Int_t class3 = hBmdNoMul->Integral(0,8);
 Int_t class4 = hBmdNoMul->Integral(0,11);
 Int_t class5 = hBmdNoMul->Integral(0,14);
 Int_t class6 = hBmdNoMul->Integral(0,18);
 Int_t class7 = hBmdNoMul->Integral(0,23);
 Int_t class8 = hBmdNoMul->Integral(0,29);
 Int_t class9 = hBmdNoMul->Integral(0,37);
 Int_t class10 = hBmdNoMul->Integral(0,100);

                    cout<<"class1 : "<<class1<<endl; 
                    cout<<"class2 : "<<class2<<endl; 
                    cout<<"class3 : "<<class3<<endl; 
                    cout<<"class4 : "<<class4<<endl; 
                    cout<<"class5 : "<<class5<<endl; 
                    cout<<"class6 : "<<class6<<endl; 
                    cout<<"class7 : "<<class7<<endl;
                    cout<<"class8 : "<<class8<<endl;
                    cout<<"class9 : "<<class9<<endl; 
                    cout<<"class10 : "<<class10<<endl; 
                 
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   /////Multiplicity Centrality classes /////////////////////////////////////////////
   TCanvas *c13 = new TCanvas("c13","Centrality classes",800,800);
   gStyle->SetOptStat(false);                                   
   //gStyle->SetPalette(1);                                       
   c13->SetRightMargin(0.0465116);
   c13->SetTopMargin(0.1);
   c13->SetFillColor(0);
  // c13->SetLogy();

   hmulclass1->Draw();  
   hmulclass2->Draw("sames");  
   hmulclass3->Draw("sames");  
   hmulclass4->Draw("sames");   
   hmulclass5->Draw("sames");  
   hmulclass6->Draw("sames"); 
   hmulclass7->Draw("sames"); 
   hmulclass8->Draw("sames"); 
   hmulclass9->Draw("sames"); 
   hmulclass10->Draw("sames"); 
  
    TLegend *leg2 = new TLegend(0.65,0.8,0.92,0.89);
   leg2->SetTextFont(62);
  // leg2->SetTextSize(1);                                    
   leg2->SetLineColor(0);
   leg2->SetLineStyle(0);
   leg2->SetLineWidth(1);
   leg2->SetFillColor(0);
   leg2->SetFillStyle(1001);
   leg2->AddEntry("","Multiplicity classes","");
   leg2->AddEntry("hmulclass1","0-10%","f");
   leg2->AddEntry("hmulclass2","10-20%","f");
   leg2->AddEntry("hmulclass3","20-30%","f");
   leg2->AddEntry("hmulclass4","30-40%","f"); 
   leg2->AddEntry("hmulclass5","40-50%","f");
   leg2->AddEntry("hmulclass6","50-60%","f");
   leg2->AddEntry("hmulclass7","60-70%","f");
   leg2->AddEntry("hmulclass8","70-80%","f");
   leg2->AddEntry("hmulclass9","80-90%","f");
   leg2->AddEntry("hmulclass10","90-100%","f");
   leg2->Draw();

 //  c13->SaveAs("mulclasses.pdf");
  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////   

  cout<<"End histograms"<<endl;
  return 0;
   
}

